/**
 * api接口统一管理
 * import API from 'API'
 * Vue.prototype.$API = API
 * Vue.use(API)
 *
 * this.$API.Login()
 */
import {fetchPost, fetchGet} from './http'
export default {
  Post (link,params) {
    return fetchPost(link,params)
  },
  Get (link,params) {
    return fetchGet(link,params)
  }
}
 