import Vue from 'vue'
import API from './index'
 
Vue.prototype.$API = API
Vue.use(API)